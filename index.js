
	
	
	// 1.  [function WITHOUT return]
	// 	Create a function which will be able to add two numbers.
	// 	-Numbers must be provided as arguments.
	// 	-Display the result of the addition in our console.
	// 	-function should only display result. It should not return anything.

	// 	Create a function which will be able to subtract two numbers.
	// 	-Numbers must be provided as arguments.
	// 	-Display the result of subtraction in our console.
	// 	-function should only display result. It should not return anything.

	// 	-invoke and pass 2 arguments to the addition function (test arguments: 5 and 15)
	// 	-invoke and pass 2 arguments to the subtraction function (test arguments:  20 and 5)
	

	function checkAddition2(num, num2){
		let number15 = num + num2;
		// let number5 = 5;
		console.log("Displayed sum of " + num + " and " + num2);
		console.log(number15);
}

	checkAddition2(15, 5);	
	
	function diffrenece1(num, num2){
		let difference = num + num2;
		console.log("Displayed difference of " + difference + " and " + num2);
		difference1 = difference - num2;
		console.log(difference1);

}
	
	
	
	diffrenece1(15, 5);
	

	
	// 2.  [function with return]	
	// 	Create a function which will be able to multiply two numbers. (test arguments: 50 and 10) 
	// 		-Numbers must be provided as arguments.
	// 		-Return the result of the multiplication.


	// 	Create a function which will be able to divide two numbers. (test arguments: 50 and 10) 
	// 		-Numbers must be provided as arguments.
	// 		-Return the result of the division.

	//  	Create a global variable called outside of the function called product.
	// 		-This product variable should be able to receive and store the result of multiplication function.
	// 	Create a global variable called outside of the function called quotient.
	// 		-This quotient variable should be able to receive and store the result of division function.

	// 	Log the value of product variable in the console.
	// 	Log the value of quotient variable in the console.

	function product(num3, num4){
		let multi = num3 * num4;
		console.log("Displayed product of " + num3 + " and " + num4);
		console.log(multi);
}
	product(50, 10);

	function quiotient(num3, num4){
		let quo = num3 / num4;
		console.log("The quotient of " + num3 + " and "+ num4);
		console.log(quo)
}
	
	
	quiotient(50, 10);


	// 3. [function with return]	
	// 	Create a function which will be able to get total area of a circle from a radius number (not variable) as argument (test argument: 15) .
	// 		-a number should be provided as an argument.
	// 		-look up the formula for calculating the area of a circle with a provided/given radius.
	// 		-look up the use of the exponent operator.
	// 		-you can save the value of the calculation in a variable.
	// 		-return the result of the area calculation.

	// 	Create a global variable called outside of the function called circleArea.
	// 		-This variable should be able to receive a result from an argument and store the result of the circle area calculation.

	// Log the value of the circleArea variable in the console.

	function circleArea(num){
		let radius = 15;
		radius = 3.1416 * radius ** 2;
		return radius;
 
	}
		let radiusArea = circleArea(15)
		console.log("The result of getting the area of a circle with 15 radius:")
	
		console.log(radiusArea);
	
	circleArea(15)


	// 4. [function with return]	
	// 	Create a function which will be able to get total average of four numbers. (test arguments: 20, 40, 60, and 80)
	// 		-4 numbers should be provided as an argument.
	// 		-look up the formula for calculating the average of numbers.
	// 		-you can save the value of the calculation in a variable.
	// 		-return the result of the average calculation.



	function checkAverage(score1, score2, score3, score4){
		let aveRagee = ((score1+score2+score3+score4)/4);
		return aveRagee; 
	}

	

	//     Create a global variable called outside of the function called averageVar.
	// 		-This variable should be able to receive arguments and store the result of the average calculation
	// 		-Log the value of the averageVar variable in the console.
	
	let averageVar = checkAverage(20, 40, 60, 80);
		console.log("The Average of 20, 40, 60, 80:");
		console.log(averageVar);

	// 5. [function with return]	
	// 	Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
	// 		-this function should take 2 numbers as an argument, your score and the total score.
	// 		-First, get the percentage of your score against the total. You can look up the formula to get percentage.
	// 		-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
	// 		-return the value of the variable isPassed.
	// 		-This function should return a boolean.

	function checkGrade(score, total){
		let scoreGrade = ((score/total)*100) >= 75;
		return scoreGrade; 
	}

		

	// 	Create a global variable called outside of the function called isPassingScore.
	// 		-This variable should be able to receive and store the boolean result of the checker function.
	// 		-Log the value of the isPassingScore variable in the console.

	let isPassingScore = checkGrade(38, 50);
		console.log("Is 38/50 a passing score?");
		console.log(isPassingScore);

